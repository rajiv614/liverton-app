export const USER_LOGIN_CHECK = 'user_login_check';
export const USER_LOGIN_SUCCESS = 'user_login_success';
export const USER_LOGIN_FAILURE = 'user_login_failure';
export const FETCH_LIST = 'fetch_list';
