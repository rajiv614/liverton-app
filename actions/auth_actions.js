

import { AsyncStorage } from 'react-native';
import { SecureStore } from 'expo'

import {
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
} from './types';

export const userLogin = (userName, password,callback) => async dispatch => {
  const securedCreds = await SecureStore.getItemAsync('credentials').catch(() => console.log('ERROR: UNABLE TO RETRIEVE SYSTEM CREDS FROM STORE'));
  const sysCreds = JSON.parse(securedCreds);

  if(userName.toLowerCase() == sysCreds.userName && password == sysCreds.pwd) {
    const token = userName; //create token
   await AsyncStorage.setItem('user_token', token);
    dispatch({ type: USER_LOGIN_SUCCESS, payload: token });
  } else {
    const error = 'Check your username/password';
    dispatch({ type: USER_LOGIN_FAILURE, errorMessage: error });
  }
  callback();
};
