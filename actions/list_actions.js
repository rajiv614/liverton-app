import axios from 'axios';
import {alertBox} from '../components/AlertBox'

import {
  FETCH_LIST
} from './types';

const LIST_ROOT_URL = 'https://jsonplaceholder.typicode.com/';


const buildListUrl = (listType) => {
  return LIST_ROOT_URL + listType.toLowerCase();
};

export const fetchLists = (listType, callback) => async (dispatch) => {
  try {
    const url = buildListUrl(listType);
    let { data } = await axios.get(url); // Make API CALL
    data={'payload':data, listName:listType}
    dispatch({ type: FETCH_LIST, listData: data });
    console.log(data);
    callback();
  } catch(e) {
    console.error(e);
    alertBox('Error', 'Error loading ' + listType);
  }
};
