import { Alert } from 'react-native'

export function alertBox(title = 'Error', content = '') {
    Alert.alert(
      title,
      content,
      [{text: 'OK'}]
  	)
}
