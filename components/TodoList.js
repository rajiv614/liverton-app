import React, {Component} from 'react';
import {Text,View, ScrollView} from 'react-native';
import {Card, CardItem, Body, CheckBox} from 'native-base'

export default class TodoList extends Component {

renderListItems (data) {

  return data.map(function(rows){
// Checkbox toggling is disabled intentionally since data is coming from api
      return(
        <Card key = {rows.id}>
          <CardItem>
            <CheckBox checked={rows.completed}
              style = {{marginRight:20, marginLeft:-10}} />
                <Text >{rows.title}</Text>
         </CardItem>
        </Card>
      );
    });
  }

  render() {
    return (
      <View>
        {this.renderListItems(this.props.listData)}
      </View>
    );
  }
}
