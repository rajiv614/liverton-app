import React, {Component} from 'react';
import {Text,View, ScrollView, Image} from 'react-native';
import {Card, CardItem,Body, } from 'native-base'

export default class AlbumList extends Component {

renderListItems (data) {
  // Returning List Items 
  return data.slice(0, 20).map(function(rows){
      return(
        <Card key = {rows.id}>
          <CardItem>
            <Image
								style={{ resizeMode: "cover", height: 150, flex:1 }}
								source={{uri: rows.thumbnailUrl }}
							/>
          </CardItem>
          <CardItem>
              <Text style ={{fontWeight: 'bold', textAlign: 'center', fontSize: 18}}>{rows.title}</Text>
          </CardItem>
        </Card>
      );
    });
  }

  render() {
    return (
      <View>
        {this.renderListItems(this.props.listData)}
      </View>
    );
  }
}
