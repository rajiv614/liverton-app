import React, {Component} from 'react';
import {Text,View, ScrollView} from 'react-native';
import {Card, CardItem,Body} from 'native-base'

export default class PostList extends Component {

renderListItems (data) {
  return data.map(function(rows){
      return(
        <Card key = {rows.id}>
          <CardItem header>
            <Text style={{ fontWeight:'bold', fontSize:18 }}>{rows.title}</Text>
          </CardItem>
          <CardItem>
            <Body>
              <Text>{rows.body}</Text>
            </Body>
          </CardItem>
        </Card>
      );
    });
  }

  render() {
    return (
      <View>
        {this.renderListItems(this.props.listData)}
      </View>
    );
  }
}
