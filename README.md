# Liverton App Test #


## Run the app on Expo  ###

* Click [Here](http://bit.ly/2x9bu8V) to run the app directly on your phone using the [Expo app](https://expo.io)


## Local Installation ##

* Navigate to this directory using the terminal and run
```
$ npm install
```
* Open Expo XDE and click open existing project
* Point expo to this directory and wait for the react native packager to start
* Click on the device button to run on ios simulator or android


## Credentials for the App ##

* username - 'livertoniscool55@hotmail.com'
* password - ''liverton'

## Libraries/Frameworks Used  ##

* Expo
* Native Base
* React Navigation
* React-Redux
* Redux
* Redux-thunk

## Notes for Interviewer ##

* This app was completed in 5 hrs and therefore has a lot of room to improve wrt styling and code re-usability
* Android testing was limited due to lack of Android Testing Environment
* Username and password for the app have been swapped as i assumed this was an error
* To-do List delibrately does not allow toggling
* I could not add unit testing using Jest as i exceeded thr 2-4 hr estimate and it  would not be representative of what i can accomplish in this time period

