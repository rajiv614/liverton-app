import {
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case USER_LOGIN_SUCCESS:
      return { token: action.payload }; // Returning Token on Successful Login
    case USER_LOGIN_FAILURE:
      return { token: null , error: action.errorMessage }; //Sending Error Object on Login Fail
    default:
      return state;
  }
}
