import {
  FETCH_LIST
} from '../actions/types';

const INITIAL_STATE = {

};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_LIST:
      return action.listData; //returning list data
    default:
      return state;
  }
}
