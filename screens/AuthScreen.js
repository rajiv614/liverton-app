import React, { Component } from 'react'
import _ from 'lodash';
import { View, Text, Image,Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux'
import {Button, Content, Form, Item, Input, Label, Spinner} from 'native-base'
import { AppLoading } from 'expo'


import * as actions from '../actions';
import {alertBox} from '../components/AlertBox'

export class AuthScreen extends Component {

  state = { userName: '', password: '', loading: false, token: null };

  async componentWillMount () {

    //Checking to see if a user is already logged in on app start up.
    let token = await AsyncStorage.getItem('user_token');
    if (token) {
      this.props.navigation.navigate('dashboard');
      this.setState({ token });
    } else {
      this.setState({ token: false });
    }
  }

  onButtonPress() {

    const { userName, password } = this.state;
    if(userName.length == 0 || password.length==0){
      alertBox('Field Empty', 'Username/Password cannot be empty')
     } else {
       this.setState({ loading: true });
       this.props.userLogin(userName,password, () => {
         this.onAuthComplete();
       });
     }
  }

  onAuthComplete() {

    if (this.props.token) {
      this.setState({
        userName: '',
        password: '',
        loading: false,
      });
      this.props.navigation.navigate('dashboard');
    } else if (this.props.error) {
        alertBox('Login Failed',this.props.error);
        this.setState({
          password: '',
          loading: false
        });
    }
  }

  render() {

    if (this.state.token == null) {
     return <AppLoading />;
    }

   if (this.state.loading) {
     return <Spinner color ='#00b0f0'/>
   }
    return (
      <Content >
        <View>
          <Image
              style={styles.imgStyle}
                source={require('../assets/images/Liverton_Logo.png')}
            />
        </View>
        <View>
          <Form>
            <Item floatingLabel>
              <Label >Username</Label>
              <Input value={this.state.userName}
                onChangeText={userName => this.setState({ userName })}
                keyboardType = 'email-address'
              />
            </Item>
            <Item last>
              <Input
                secureTextEntry
                placeholder="Password"
                value={this.state.password}
                onChangeText={password => this.setState({ password })}
              />
            </Item>
          </Form>
      <Button style={styles.loginButton} onPress={this.onButtonPress.bind(this)}>
      <Text style={styles.btnText}>Sign In</Text>
      </Button>

      </View>
    </Content>
    );
  }
}
function mapStateToProps({ auth }) {
  return { token: auth.token, error: auth.error };
}

const styles = {
  loginButton: {
    marginTop:25,
    width:100,
    justifyContent: 'center',
    backgroundColor:'#00b0f0',
    alignSelf:'center'
  },
  btnText:{
    fontWeight:'bold',
    color:'white'
  },
  imgStyle:{
    resizeMode: "contain",
     alignSelf:'center',
     width:250,
     flex: 1,
     marginTop:80
  }
}

export default connect(mapStateToProps, actions)(AuthScreen);
