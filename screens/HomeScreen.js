import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image,AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Button, Content, Icon } from 'native-base';

import * as actions from '../actions';

class HomeScreen extends Component {

  static navigationOptions = {
      title: 'Dashboard',
      headerLeft: null //Disablong Back Button for Dashboard
  }

  onButtonPress = (name) => {
      this.props.navigation.navigate('lists',{title:name});
  }
  logOut () {
    AsyncStorage.removeItem('user_token').then(() => this.props.navigation.navigate('auth'));
  }

  render() {

    return (
      <Content style={styles.contentStyle}>
      <View >
        <Image
          style={styles.imgStyle}
              source={require('../assets/images/Liverton_Logo.png')}
          />
      </View>

      <View style ={{alignSelf:'center', justifyContent:'center'}}>
        <Button iconRight style = {styles.dashboardButton} onPress= {this.onButtonPress.bind(this,'Posts')} >
          <Icon name='ios-paper'/>
          <Text style= {styles.btnText}>Posts</Text>
        </Button>

        <Button iconRight style = {styles.dashboardButton} onPress= {this.onButtonPress.bind(this,'Photos')}>
          <Icon name='md-photos'/>
          <Text style= {styles.btnText}>Photos</Text>
        </Button>

        <Button iconRight style = {styles.dashboardButton} onPress= {this.onButtonPress.bind(this,'Todos')}>
          <Icon name='md-checkbox'/>
          <Text style= {styles.btnText}>Todos</Text>
        </Button>

      </View>

      <Button full
        onPress = {this.logOut.bind(this)} style={{marginTop:50, backgroundColor:'#00b0f0'}}>
        <Icon name='md-log-out' />
        <Text style={{ color:'white', fontWeight:'bold'}}>Sign Out</Text>
      </Button>

    </Content>
    );
  }
}
const styles = {
  dashboardButton: {
    marginTop:25,
    width:100,
    justifyContent: 'center',
    backgroundColor:'#00b0f0'
  },
  btnText:{
    fontWeight:'bold',
    color:'white'
  },
  imgStyle: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width:250,
    marginTop:50
  },
  contentStyle:{
    backgroundColor: '#f5f5f5',
    flex:1
  }
}
export default connect(null, actions)(HomeScreen);
