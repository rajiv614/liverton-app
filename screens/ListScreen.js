import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Content, Spinner } from 'native-base';

import * as actions from '../actions';
import  PostList  from '../components/PostList'
import PhotoList from '../components/AlbumList'
import TodoList from '../components/TodoList'

export class ListScreen extends Component {

componentWillMount() {
  this.props.listType = "loading"
  this.props.fetchLists(this.props.navigation.state.params.title, () => {
  });
}

  static navigationOptions = ({ navigation }) => ({
     title: navigation.state.params.title //Setting Appropriate header title
   });


  renderList (listType) {

    switch (listType) {
      case 'Posts':
        return  <PostList listData = {this.props.listContent} />
      case 'Photos':
        return <PhotoList listData =  {this.props.listContent} />
      case 'Todos':
        return <TodoList listData =  {this.props.listContent} />
        case 'loading':
        return <Spinner color ='#00b0f0'/>
    }
  }

  render() {
    return (
      <Content>
      {this.renderList(this.props.listType)}
      </Content>
    );
  }
}

function mapStateToProps({ lists }) {
  return { listContent: lists.payload, listType:lists.listName };
}

export default connect(mapStateToProps, actions)(ListScreen);
