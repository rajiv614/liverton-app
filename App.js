import React from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';
import {StackNavigator, TabNavigator} from 'react-navigation'
import { Provider } from 'react-redux'
import { SecureStore } from 'expo'

import store from './store';
import AuthScreen from './screens/AuthScreen';
import HomeScreen from './screens/HomeScreen';
import ListScreen from './screens/ListScreen';

export default class App extends React.Component {

componentWillMount () {

  let credentials = '{"userName":"livertoniscool55@hotmail.com", "pwd":"liverton"}';
  SecureStore.setItemAsync('credentials', credentials).then(() => console.log('credentials stored: ')); //Saving Generic Creds To SecureStore

}


  render() {
    //Configuring Navigator(React Navigation)
    const MainNavigator = TabNavigator({
        auth: { screen: AuthScreen },
        main: {screen: StackNavigator({
          dashboard: {screen: HomeScreen },
          lists: {screen: ListScreen}
        },{
          navigationOptions:{
            headerBackTitle:'Back',
            headerStyle: {
              backgroundColor:'#00b0f0'
            },
            headerBackTitleStyle :{
              color:'white'
            },
            headerTitleStyle :{
              color:'white'
            },
            headerTintColor: 'white',
          },
        })}
      },{
        navigationOptions:{
          tabBarVisible:false
      }});

    return (
      <Provider store={store}>
        <MainNavigator />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
